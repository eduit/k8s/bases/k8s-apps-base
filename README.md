# k8s-apps-base

Basic (cluster implementation agnostic) part of application configuration for clusterwide configuration objects. These blueprints can be used as kustomization bases in cluster specific repositories. 

## Kustomization

Cluster components are arranged in subdirectories. Each subdirectory must contain a `kustomization.yml` file. `kustomize` uses multiple YAML files and merges them into a single YAML, with the option to change or add parts.

[kustomize site](https://kubernetes-sigs.github.io/kustomize/)
[kustomize reference](https://kubectl.docs.kubernetes.io/references/kustomize/)

## Usage

Create a git repository for a specific cluster. Add this repository as a git submodule:

```
git submodule add https://gitlab.ethz.ch/eduit/k8s/bases/k8s-apps-base.git k8s-apps-base-main
```

If desired, you may use multiple clones with different references to tags or branches:

```
git submodule add -b feature https://gitlab.ethz.ch/eduit/k8s/bases/k8s-apps-base.git k8s-apps-base-feature

```

But be aware: flexibility comes with complexity...

Now the submodules can be used in a `kustomization.yml`:

```
resources:
  - ./k8s-apps-base-main/op-portal
  - my-additional-declaration.yml
```
