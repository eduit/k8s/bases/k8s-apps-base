---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: frontend-deployment
  namespace: exam-portal
spec:
  selector:
    matchLabels:
      app: frontend
  template:
    metadata:
      labels:
        app: frontend
        app.kubernetes.io/name: frontend
        app.kubernetes.io/part-of: frontend
        app.kubernetes.io/version: v1.0.0
    spec:
      initContainers:
      - name: check-nats-ready
        image: curlimages/curl:latest
        env:
        - name: NATS_HOST
          valueFrom:
            secretKeyRef:
              name: exam-portal-nats-credentials
              key: NATS_HOST
        command:
        - /bin/sh
        - -c
        - |
          until curl -s http://$NATS_HOST:8222/healthz | grep -q '"status":"ok"'; do
            echo "Waiting for NATS to be ready..."
            sleep 2
          done
      containers:
      - name: frontend
        image: "registry.ethz.ch/eduit/apps/exam-portal/frontend:latest"
        imagePullPolicy: "Always"
        resources:
          limits:
            memory: "256Mi"
            cpu: "500m"
          requests:
            memory: "128Mi"
            cpu: "100m"
        env:
        - name: POD_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name
        - name: POD_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: POD_NODENAME
          valueFrom:
            fieldRef:
              fieldPath: spec.nodeName
        - name: POD_IP
          valueFrom:
            fieldRef:
              fieldPath: status.podIP
        - name: POD_SERVICE_ACCOUNT
          valueFrom:
            fieldRef:
              fieldPath: spec.serviceAccountName
        - name: NATS_HOST
          valueFrom:
            secretKeyRef:
              name: exam-portal-nats-credentials
              key: NATS_HOST
        - name: NATS_PORT
          valueFrom:
            secretKeyRef:
              name: exam-portal-nats-credentials
              key: NATS_PORT
        - name: AMT_JOB_URL
          value: http://amt-job.$(POD_NAMESPACE).svc.cluster.local
        - name: AMT_SERVICE_URL
          value: http://amt-service.$(POD_NAMESPACE).svc.cluster.local
        - name: ANSIBLE_JOB_URL
          value: http://ansible-job.$(POD_NAMESPACE).svc.cluster.local
        - name: ANSIBLE_SERVICE_URL
          value: http://ansible-service.$(POD_NAMESPACE).svc.cluster.local
        - name: AUTHORITY_SERVICE_URL
          value: http://authority-service.$(POD_NAMESPACE).svc.cluster.local
        - name: BOOTROBOTER_SERVICE_URL
          value: http://bootroboter-service.$(POD_NAMESPACE).svc.cluster.local
        - name: CALENDAR_SERVICE_URL
          value: http://calendar-service.$(POD_NAMESPACE).svc.cluster.local
        - name: FRONTEND_URL
          value: http://frontend.$(POD_NAMESPACE).svc.cluster.local
        - name: LOG_COLLECTOR_SERVICE_URL
          value: http://log-collector-service.$(POD_NAMESPACE).svc.cluster.local
        - name: NOTIFICATIONS_SERVICE_URL
          value: http://notifications-service.$(POD_NAMESPACE).svc.cluster.local
        - name: OTRS_SERVICE_URL
          value: http://otrs-service.$(POD_NAMESPACE).svc.cluster.local
        readinessProbe:
          initialDelaySeconds: 15
          periodSeconds: 30
          httpGet:
            path: /health/readiness
            port: 8080
            scheme: HTTP
          timeoutSeconds: 2
          failureThreshold: 1
          successThreshold: 1
        livenessProbe:
          initialDelaySeconds: 30
          periodSeconds: 30
          httpGet:
            path: /health/liveness
            port: 8080
            scheme: HTTP
          timeoutSeconds: 5
          failureThreshold: 10
          successThreshold: 1
        ports:
        - containerPort: 8080
